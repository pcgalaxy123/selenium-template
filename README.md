# Selenium Edu

## process.env

Place `.env` file in root of project

The containing should be like this:

```ini
VARIABLE=""
```

## Run

To run the project,

- Setup Google Chrome and Chrome Driver
```sh
brew install google-chrome chromedriver
```
- Install dependencies
```sh
npm i
```
- Run command to start tests
```sh
npm run test
```
- Run specific test
```sh
npm run test -- -g '[Describe name] Test name'
```
- Run lint
```sh
npm run eslint
npm run prettier
```
- Run lint fix
```sh
npm run lint:fix
```
- Generate standalone allure report
```sh
npm run generate
```
- Serve standalone report
```sh
npm run serve
```

## Deploy on firebase
https://firebase.google.com/docs/hosting/quickstart