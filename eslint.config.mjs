import eslint from '@eslint/js';
import eslintprettier from 'eslint-config-prettier';
import eslintUnusedImports from 'eslint-plugin-unused-imports';
import tseslint from 'typescript-eslint';

export default [
  ...tseslint.config(
    eslint.configs.recommended,
    ...tseslint.configs.recommended
  ),
  eslintprettier,
  {
    plugins: {
      eslintImports: eslintUnusedImports,
    },
    rules: {
      '@typescript-eslint/no-unused-vars': 'off',
      'eslintImports/no-unused-imports': 'error',
      'eslintImports/no-unused-vars': [
        'warn',
        {
          vars: 'all',
          varsIgnorePattern: '^_',
          args: 'after-used',
          argsIgnorePattern: '^_',
        },
      ],
    },
  },
];
