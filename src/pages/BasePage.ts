import { type WebDriver } from 'selenium-webdriver';

import { PageControl } from '@/util/PageControl';

export class BasePage {
  control: PageControl;

  constructor(
    private readonly url: string,
    protected readonly driver: WebDriver
  ) {
    this.control = new PageControl(driver);
  }

  async open() {
    await this.driver.get(this.url);
  }

  async close() {
    await this.driver.quit();
  }
}
