import { By } from 'selenium-webdriver';

export const GOOGLE_LOCATORS = {
  searchBox: () => By.css('textarea'),
};
