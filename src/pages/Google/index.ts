import { By, Key, type WebDriver } from 'selenium-webdriver';

import { BasePage } from '@/pages/BasePage';

export class GooglePage extends BasePage {
  constructor(driver: WebDriver) {
    super('https://google.com', driver);
  }

  async search() {
    const element = await this.control.locateElement(By.css('textarea'));
    await element.sendKeys('selenium');
    await element.sendKeys(Key.ENTER);
  }
}
