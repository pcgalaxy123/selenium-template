import { GooglePage } from '@/pages/Google';
import { Driver } from '@/util/Driver';

describe('Opens google page', function () {
  let page: GooglePage;

  beforeEach(async () => {
    const driver = new Driver().build().forPipeline();
    page = new GooglePage(driver);
    await page.open();
  });

  it('Open ', async () => {
    await page.search();
  });

  afterEach(async () => {
    await page.close();
  });
});
