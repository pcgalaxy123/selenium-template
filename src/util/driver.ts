import { Builder, type WebDriver } from 'selenium-webdriver';
import { Options } from 'selenium-webdriver/chrome';

export class Driver {
  public build() {
    return {
      forPipeline: (): WebDriver => {
        const chromeOptions = new Options();
        chromeOptions.addArguments('--disable-notifications');
        chromeOptions.addArguments('start-maximized');
        chromeOptions.addArguments('--log-level=DEBUG');
        chromeOptions.addArguments('--disable-gpu');
        chromeOptions.addArguments('--no-sandbox');
        chromeOptions.addArguments('--headless');
        chromeOptions.addArguments('--disable-dev-shm-usage');

        const driver = new Builder()
          .forBrowser('chrome')
          .setChromeOptions(chromeOptions)
          .build();

        return driver;
      },
      custom: (options: Options = new Options()): WebDriver => {
        const driver = new Builder()
          .forBrowser('chrome')
          .setChromeOptions(options)
          .build();

        return driver;
      },
    };
  }
}
