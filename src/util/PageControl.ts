import { allure } from 'allure-mocha/runtime';
import {
  type By,
  type WebDriver,
  type WebElement,
  until,
} from 'selenium-webdriver';

import { TIMEOUT } from '@/constants';

interface LocateElementOptions {
  visible?: boolean;
  enabled?: boolean;
  timeout?: number;
  throwError?: boolean;
}

export class PageControl {
  constructor(private readonly driver: WebDriver) {}

  async checkElement(element: WebElement, opts?: LocateElementOptions) {
    if (opts?.visible !== undefined) {
      if (opts.visible)
        element = await this.driver.wait(
          until.elementIsVisible(element),
          opts.timeout ?? TIMEOUT
        );
      else if (!opts.visible)
        element = await this.driver.wait(
          until.elementIsNotVisible(element),
          opts.timeout ?? TIMEOUT
        );
    }

    if (opts?.enabled !== undefined) {
      if (opts.enabled)
        element = await this.driver.wait(
          until.elementIsEnabled(element),
          opts.timeout ?? TIMEOUT
        );
      else if (!opts.enabled)
        element = await this.driver.wait(
          until.elementIsDisabled(element),
          opts.timeout ?? TIMEOUT
        );
    }

    return element;
  }

  async locateElement(
    by: By,
    opts?: LocateElementOptions
  ): Promise<WebElement> {
    const element = await this.driver.wait(
      until.elementLocated(by),
      opts?.timeout ?? TIMEOUT
    );

    return await this.checkElement(element, opts);
  }

  async locateElements(by: By): Promise<WebElement[]> {
    return await this.driver.wait(until.elementsLocated(by), TIMEOUT);
  }

  async getPathname(): Promise<string> {
    const url = await this.driver.getCurrentUrl();
    return '/' + url.split('/').slice(3).join('/');
  }

  async screenshot() {
    const png = await this.driver.takeScreenshot();
    allure.attachment('Screenshot', Buffer.from(png, 'base64'), 'image/png');
  }

  async isElementExist(by: By, opts?: LocateElementOptions) {
    let element = null;
    try {
      element = await this.locateElement(by, opts);
    } catch (err) {
      element = null;
    }
    return !(element == null);
  }
}
